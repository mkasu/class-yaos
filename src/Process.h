#ifndef PROCESS_H
#define PROCESS_H

#include "Interrupt.h"
#include "types.h"
#include "Memory.h"

struct task {
        struct cpu_state*       cpu_state;
        struct task*            next;
        struct vmm_context*     context;
};


class Process {
public:
        Process();
        ~Process();
        struct task* init_task(void (*entry)());
private:
        uint8_t* stack;
        uint8_t* user_stack;
        struct task* task;
};

struct cpu_state* schedule(struct cpu_state* cpu);
void init_multitasking();
void change_task_context(); 

#endif
