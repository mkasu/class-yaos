#include "Memory.h"
#include "Video.h"

#define BITMAP_SIZE 32768
static uint32_t bitmap[BITMAP_SIZE];

extern "C" int kernel_start;
extern "C" int kernel_end;


void* pmm_alloc()
{
        int i,j;

        for(i = 0; i < BITMAP_SIZE; i++) {
                if(bitmap[i] != 0) {
                        for(j = 0; j < 32; j++) {
                                if(bitmap[i] & (1 << j)) {
                                        bitmap[i] &= ~(1 << j);
                                        return (void*)( (i*32+j) * 4096);
                                }
                        }
                }
        }

        return NULL;
}

void pmm_mark_used(void* page)
{
        uintptr_t index = (uintptr_t) page / 4096;
        bitmap[index/32] &= ~(1 << (index % 32));
}

void pmm_free(void* page)
{
        uintptr_t index = (uintptr_t) page / 4096;
        bitmap[index/32] |= (1 << (index % 32));
}

void init_pmm(const Multiboot multiboot_structure)
{
        screen << "Initialize: PMM  ... ";
        struct Multiboot_mmap* mmap = multiboot_structure.mbs_mmap_addr;
        struct Multiboot_mmap* mmap_end = (Multiboot_mmap*)
                ((uintptr_t) multiboot_structure.mbs_mmap_addr + multiboot_structure.mbs_mmap_length);
        screen << "1 ";
        unsigned char* p = (unsigned char*)bitmap;
        for(int i = 0; i < sizeof(bitmap); i++) {
                *p = 0;
                p++;
        }
        screen << "2 ";
        // mark everything whats in bios memory map free as free
        while(mmap < mmap_end) {
                if(mmap->type == 1) {
                        uintptr_t addr = mmap->base;
                        uintptr_t end_addr = addr + mmap->length;
                        
                        while(addr < end_addr) {
                                pmm_free((void*) addr);
                                addr += 0x1000;
                        }
                }
                mmap++;
        }
        screen << "3 ";
        // mark kernel as used memory
        uintptr_t addr = (uintptr_t) &kernel_start;
        while(addr < (uintptr_t) &kernel_end) {
                pmm_mark_used((void*) addr);
                addr+= 0x1000;
        }

        screen << "done!\n";
}


struct vmm_context* vmm_create_context()
{
        struct vmm_context* context = (vmm_context*)pmm_alloc();
        int i;

        context->pagedir = (uint32_t*)pmm_alloc();
        for(i = 0; i < 1024; i++) {
                context->pagedir[i] = 0;
        }

        return context;
}

int vmm_map_page(struct vmm_context* context, uintptr_t virt, uintptr_t phys) 
{
        uint32_t page_index = virt / 0x1000;
        uint32_t pd_index = page_index / 1024;
        uint32_t pt_index = page_index % 1024;

        uint32_t* page_table;
        int i;

        if ((virt & 0xFFF) || (phys & 0xFFF)) {
                return -1;
        }

        if (context->pagedir[pd_index] & PTE_PRESENT) {
                page_table = (uint32_t*) (context->pagedir[pd_index] & ~0xFFF);
        } else {
                page_table = (uint32_t*)pmm_alloc();
                for (i = 0; i < 1024; i++) {
                        page_table[i] = 0;
                }
                context->pagedir[pd_index] =
                (uint32_t) page_table | PTE_PRESENT | PTE_WRITE;
        }

        page_table[pt_index] = phys | PTE_PRESENT | PTE_WRITE;
        asm volatile("invlpg %0" : : "m" (*(char*)virt));
}

void vmm_activate_context(struct vmm_context* context)
{
        asm volatile("mov %0, %%cr3" : : "r" (context->pagedir));
}

static struct vmm_context* kernel_context;

void init_vmm()
{
        screen << "Initialize: VMM  ... ";
        uint32_t cr0;
        int i;

        kernel_context = vmm_create_context();

        for(i=0; i < 4096*1024; i += 0x1000) {
                vmm_map_page(kernel_context, i, i);
        }

        vmm_activate_context(kernel_context);

        asm volatile("mov %%cr0, %0" : "=r" (cr0));
        cr0 |= (1 << 31);
        asm volatile("mov %0, %%cr0" : : "r" (cr0));
        screen << "done!\n";
}
