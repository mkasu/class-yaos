#ifndef MEMORY_H
#define MEMORY_H

#include "types.h"
#include "Multiboot.h"

#define PTE_PRESENT 0x1
#define PTE_WRITE   0x2
#define PTE_USER    0x4

void* pmm_alloc();
void pmm_mark_used(void* page);
void pmm_free(void* page);
void init_pmm(const Multiboot multiboot_structure);
void init_vmm();

struct vmm_context 
{
        uint32_t* pagedir;
        uint32_t* pagedir_phys;
};

#endif
