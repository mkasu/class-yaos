#include "types.h"
#include "Interrupt.h"
#include "Process.h"
#include "Video.h"

extern "C" struct cpu_state* handle_interrupt(struct cpu_state* cpu);

// Exceptions
extern "C" void intr_stub_0(void);
extern "C" void intr_stub_1(void);
extern "C" void intr_stub_2(void);
extern "C" void intr_stub_3(void);
extern "C" void intr_stub_4(void);
extern "C" void intr_stub_5(void);
extern "C" void intr_stub_6(void);
extern "C" void intr_stub_7(void);
extern "C" void intr_stub_8(void);
extern "C" void intr_stub_9(void);
extern "C" void intr_stub_10(void);
extern "C" void intr_stub_11(void);
extern "C" void intr_stub_12(void);
extern "C" void intr_stub_13(void);
extern "C" void intr_stub_14(void);
extern "C" void intr_stub_15(void);
extern "C" void intr_stub_16(void);
extern "C" void intr_stub_17(void);
extern "C" void intr_stub_18(void);

// IRQs
extern "C" void intr_stub_32(void);
extern "C" void intr_stub_33(void);
extern "C" void intr_stub_34(void);
extern "C" void intr_stub_35(void);
extern "C" void intr_stub_36(void);
extern "C" void intr_stub_37(void);
extern "C" void intr_stub_38(void);
extern "C" void intr_stub_39(void);
extern "C" void intr_stub_40(void);
extern "C" void intr_stub_41(void);
extern "C" void intr_stub_42(void);
extern "C" void intr_stub_43(void);
extern "C" void intr_stub_44(void);
extern "C" void intr_stub_45(void);
extern "C" void intr_stub_46(void);
extern "C" void intr_stub_47(void);

// Syscall
extern "C" void intr_stub_48(void);

#define GDT_FLAG_DATASEG 0x02
#define GDT_FLAG_CODESEG 0x0a
#define GDT_FLAG_TSS     0x09
 
#define GDT_FLAG_SEGMENT 0x10
#define GDT_FLAG_RING0   0x00
#define GDT_FLAG_RING3   0x60
#define GDT_FLAG_PRESENT 0x80
 
#define GDT_FLAG_4K_GRAN 0x800
#define GDT_FLAG_32_BIT  0x400

#define IDT_FLAG_INTERRUPT_GATE 0xe
#define IDT_FLAG_PRESENT 0x80
#define IDT_FLAG_RING0 0x00
#define IDT_FLAG_RING3 0x60

#define GDT_ENTRIES 5
static uint64_t gdt[GDT_ENTRIES];
static uint32_t tss[32] = { 0,0, 0x10 };

#define IDT_ENTRIES 256
static long long unsigned int idt[IDT_ENTRIES];

static void gdt_set_entry(int i, unsigned int base, unsigned int limit, int flags);

void init_gdt() 
{
        screen << "Initialize: GDT  ... ";
        struct {
                uint16_t limit;
                void* pointer;
        } __attribute__((packed)) gdtp;
        
        gdtp.limit = GDT_ENTRIES * 8 -1,
        gdtp.pointer = gdt;

        gdt_set_entry(0, 0, 0, 0);
        gdt_set_entry(1, 0, 0xfffff, GDT_FLAG_SEGMENT | GDT_FLAG_32_BIT | GDT_FLAG_CODESEG | GDT_FLAG_4K_GRAN | GDT_FLAG_PRESENT);
        gdt_set_entry(2, 0, 0xfffff, GDT_FLAG_SEGMENT | GDT_FLAG_32_BIT | GDT_FLAG_DATASEG | GDT_FLAG_4K_GRAN | GDT_FLAG_PRESENT);
        gdt_set_entry(3, 0, 0xfffff, GDT_FLAG_SEGMENT | GDT_FLAG_32_BIT | GDT_FLAG_CODESEG | GDT_FLAG_4K_GRAN | GDT_FLAG_PRESENT | GDT_FLAG_RING3);
        gdt_set_entry(4, 0, 0xfffff, GDT_FLAG_SEGMENT | GDT_FLAG_32_BIT | GDT_FLAG_DATASEG | GDT_FLAG_4K_GRAN | GDT_FLAG_PRESENT | GDT_FLAG_RING3);
        gdt_set_entry(5, (uint32_t) tss, sizeof(tss), GDT_FLAG_TSS | GDT_FLAG_PRESENT | GDT_FLAG_RING3);

        asm volatile("lgdt %0" : : "m" (gdtp));

        asm volatile(
                "mov $0x10, %ax;"
                "mov %ax, %ds;"
                "mov %ax, %es;"
                "mov %ax, %ss;"
                "ljmp $0x8, $.1;"
                ".1:"
        );

        screen << "done!\n";
}

static void gdt_set_entry(int i, unsigned int base, unsigned int limit, int flags) {
        gdt[i] = limit & 0xffffLL;
        gdt[i] |= (base & 0xffffffLL) << 16;
        gdt[i] |= (flags & 0xffLL) << 40;
        gdt[i] |= ((limit >> 16) & 0xfLL) << 48;
        gdt[i] |= ((flags >> 8)& 0xffLL) << 52;
        gdt[i] |= ((base >> 24) & 0xffLL) << 56;
}

static void idt_set_entry(int i, void (*fn)(), unsigned int selector, int flags)
{
        unsigned long int handler = (unsigned long int) fn;
        idt[i] = handler & 0xffffLL;
        idt[i] |= (selector & 0xffffLL) << 16;
        idt[i] |= (flags & 0xffLL) << 40;
        idt[i] |= ((handler>>16) & 0xffffLL) << 48;
}

static inline void outb(uint16_t port, uint8_t data)
{
        asm volatile ("outb %0, %1" : : "a" (data), "Nd" (port));
}

static void init_pic() 
{
        screen << "Initialize: PIC  ... ";
        // initialize master pic
        outb(0x20, 0x11); // init command for pic
        outb(0x21, 0x20); // interrupt for irq 0
        outb(0x21, 0x04); // slave at irq 2
        outb(0x21, 0x01); // icw 4

        // initialize slave pic
        outb(0xa0, 0x11); // init command for pic
        outb(0xa1, 0x28); // interrupt for irq 8
        outb(0xa1, 0x02); // slave at irq 2
        outb(0xa1, 0x01); // icw 4
        
        // activate all irq
        outb(0x20, 0x0);
        outb(0xa0, 0x0);
        screen << "done!\n";
}

void init_interrupts()
{
        screen << "Initialize: IDT  ... ";
        struct {
                unsigned short int limit;
                void* pointer;
        } __attribute__((packed)) idtp;

        idtp.limit = IDT_ENTRIES * 8 - 1;
        idtp.pointer = idt;

        // Exceptions
        idt_set_entry(0, intr_stub_0, 0x8, IDT_FLAG_INTERRUPT_GATE | IDT_FLAG_RING0 | IDT_FLAG_PRESENT);
        idt_set_entry(1, intr_stub_1, 0x8, IDT_FLAG_INTERRUPT_GATE | IDT_FLAG_RING0 | IDT_FLAG_PRESENT);
        idt_set_entry(2, intr_stub_2, 0x8, IDT_FLAG_INTERRUPT_GATE | IDT_FLAG_RING0 | IDT_FLAG_PRESENT);
        idt_set_entry(3, intr_stub_3, 0x8, IDT_FLAG_INTERRUPT_GATE | IDT_FLAG_RING0 | IDT_FLAG_PRESENT);
        idt_set_entry(4, intr_stub_4, 0x8, IDT_FLAG_INTERRUPT_GATE | IDT_FLAG_RING0 | IDT_FLAG_PRESENT);
        idt_set_entry(5, intr_stub_5, 0x8, IDT_FLAG_INTERRUPT_GATE | IDT_FLAG_RING0 | IDT_FLAG_PRESENT);
        idt_set_entry(6, intr_stub_6, 0x8, IDT_FLAG_INTERRUPT_GATE | IDT_FLAG_RING0 | IDT_FLAG_PRESENT);
        idt_set_entry(7, intr_stub_7, 0x8, IDT_FLAG_INTERRUPT_GATE | IDT_FLAG_RING0 | IDT_FLAG_PRESENT);
        idt_set_entry(8, intr_stub_8, 0x8, IDT_FLAG_INTERRUPT_GATE | IDT_FLAG_RING0 | IDT_FLAG_PRESENT);
        idt_set_entry(9, intr_stub_9, 0x8, IDT_FLAG_INTERRUPT_GATE | IDT_FLAG_RING0 | IDT_FLAG_PRESENT);
        idt_set_entry(10, intr_stub_10, 0x8, IDT_FLAG_INTERRUPT_GATE | IDT_FLAG_RING0 | IDT_FLAG_PRESENT);
        idt_set_entry(11, intr_stub_11, 0x8, IDT_FLAG_INTERRUPT_GATE | IDT_FLAG_RING0 | IDT_FLAG_PRESENT);
        idt_set_entry(12, intr_stub_12, 0x8, IDT_FLAG_INTERRUPT_GATE | IDT_FLAG_RING0 | IDT_FLAG_PRESENT);
        idt_set_entry(13, intr_stub_13, 0x8, IDT_FLAG_INTERRUPT_GATE | IDT_FLAG_RING0 | IDT_FLAG_PRESENT);
        idt_set_entry(14, intr_stub_14, 0x8, IDT_FLAG_INTERRUPT_GATE | IDT_FLAG_RING0 | IDT_FLAG_PRESENT);
        idt_set_entry(15, intr_stub_15, 0x8, IDT_FLAG_INTERRUPT_GATE | IDT_FLAG_RING0 | IDT_FLAG_PRESENT);
        idt_set_entry(16, intr_stub_16, 0x8, IDT_FLAG_INTERRUPT_GATE | IDT_FLAG_RING0 | IDT_FLAG_PRESENT);
        idt_set_entry(17, intr_stub_17, 0x8, IDT_FLAG_INTERRUPT_GATE | IDT_FLAG_RING0 | IDT_FLAG_PRESENT);
        idt_set_entry(18, intr_stub_18, 0x8, IDT_FLAG_INTERRUPT_GATE | IDT_FLAG_RING0 | IDT_FLAG_PRESENT);

        // IRQs
        idt_set_entry(32, intr_stub_32, 0x8, IDT_FLAG_INTERRUPT_GATE | IDT_FLAG_RING0 | IDT_FLAG_PRESENT);
        idt_set_entry(33, intr_stub_33, 0x8, IDT_FLAG_INTERRUPT_GATE | IDT_FLAG_RING0 | IDT_FLAG_PRESENT);
        idt_set_entry(34, intr_stub_34, 0x8, IDT_FLAG_INTERRUPT_GATE | IDT_FLAG_RING0 | IDT_FLAG_PRESENT);
        idt_set_entry(35, intr_stub_35, 0x8, IDT_FLAG_INTERRUPT_GATE | IDT_FLAG_RING0 | IDT_FLAG_PRESENT);
        idt_set_entry(36, intr_stub_36, 0x8, IDT_FLAG_INTERRUPT_GATE | IDT_FLAG_RING0 | IDT_FLAG_PRESENT);
        idt_set_entry(37, intr_stub_37, 0x8, IDT_FLAG_INTERRUPT_GATE | IDT_FLAG_RING0 | IDT_FLAG_PRESENT);
        idt_set_entry(38, intr_stub_38, 0x8, IDT_FLAG_INTERRUPT_GATE | IDT_FLAG_RING0 | IDT_FLAG_PRESENT);
        idt_set_entry(39, intr_stub_39, 0x8, IDT_FLAG_INTERRUPT_GATE | IDT_FLAG_RING0 | IDT_FLAG_PRESENT);
        idt_set_entry(40, intr_stub_40, 0x8, IDT_FLAG_INTERRUPT_GATE | IDT_FLAG_RING0 | IDT_FLAG_PRESENT);
        idt_set_entry(41, intr_stub_41, 0x8, IDT_FLAG_INTERRUPT_GATE | IDT_FLAG_RING0 | IDT_FLAG_PRESENT);
        idt_set_entry(42, intr_stub_42, 0x8, IDT_FLAG_INTERRUPT_GATE | IDT_FLAG_RING0 | IDT_FLAG_PRESENT);
        idt_set_entry(43, intr_stub_43, 0x8, IDT_FLAG_INTERRUPT_GATE | IDT_FLAG_RING0 | IDT_FLAG_PRESENT);
        idt_set_entry(44, intr_stub_44, 0x8, IDT_FLAG_INTERRUPT_GATE | IDT_FLAG_RING0 | IDT_FLAG_PRESENT);
        idt_set_entry(45, intr_stub_45, 0x8, IDT_FLAG_INTERRUPT_GATE | IDT_FLAG_RING0 | IDT_FLAG_PRESENT);
        idt_set_entry(46, intr_stub_46, 0x8, IDT_FLAG_INTERRUPT_GATE | IDT_FLAG_RING0 | IDT_FLAG_PRESENT);
        idt_set_entry(47, intr_stub_47, 0x8, IDT_FLAG_INTERRUPT_GATE | IDT_FLAG_RING0 | IDT_FLAG_PRESENT);

        // Syscalls
        idt_set_entry(48, intr_stub_48, 0x8, IDT_FLAG_INTERRUPT_GATE | IDT_FLAG_RING3 | IDT_FLAG_PRESENT);
        
        screen << "done!\n";

        init_pic();
	
	asm volatile("lidt %0" : : "m" (idtp));
        asm volatile("sti");
}

struct cpu_state* handle_syscall(struct cpu_state* cpu)
{
    switch (cpu->eax) {
        case 0:
            screen << cpu->ebx;
            break;
    }
 
    return cpu;
}

struct cpu_state* handle_irq(struct cpu_state* cpu)
{
        struct cpu_state* new_cpu = cpu;
        
        if(cpu->intr == 0x20) {
                new_cpu = schedule(cpu);
        }
        if(cpu->intr >= 0x28) {
                outb(0xa0, 0x20);
        }
        outb(0x20, 0x20);

        return new_cpu;
}

void handle_exception(struct cpu_state* cpu) 
{
        screen << "Exception " << (int)cpu->intr;
                
        while(1) {
                asm volatile("cli; hlt");
        }
}

struct cpu_state* handle_interrupt(struct cpu_state* cpu)
{
        struct cpu_state* new_cpu = cpu;

        if(cpu->intr <= 0x1f) { 
                // Exception Handling
                handle_exception(cpu);
        } else if(cpu->intr >= 0x20 && cpu->intr <= 0x2f) { 
                // IRQ Handling
                new_cpu = handle_irq(cpu);
                tss[1] = (uint32_t) (new_cpu + 1);
        } else if(cpu->intr == 0x30) {
                // Syscall Handling
                new_cpu = handle_syscall(cpu);
        } else {
                // Should not happen
                screen << "Unknown interrupt " << (int)cpu->intr;
        }

        // Paging
        if(cpu != new_cpu) {
                change_task_context();
        }

        return new_cpu;
}


