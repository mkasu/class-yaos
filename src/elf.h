#ifndef ELF_H
#define ELF_H
 
#include "types.h"
 
#define ELF_MAGIC 0x464C457F
 
struct elf_header {
    uint32_t    magic;
    uint32_t    version;
    uint64_t    reserved;
    uint64_t    version2;
    uint32_t    entry;
    uint32_t    ph_offset;
    uint32_t    sh_offset;
    uint32_t    flags;
    uint16_t    header_size;
    uint16_t    ph_entry_size;
    uint16_t    ph_entry_count;
    uint16_t    sh_entry_size;
    uint16_t    sh_entry_count;
    uint16_t    sh_str_table_index;
} __attribute__((packed));
 
struct elf_program_header {
    uint32_t    type;
    uint32_t    offset;
    uint32_t    virt_addr;
    uint32_t    phys_addr;
    uint32_t    file_size;
    uint32_t    mem_size;
    uint32_t    flags;
    uint32_t    alignment;
} __attribute__((packed));

void init_elf(elf_header* image)
{
    struct elf_header* header = image;
    struct elf_program_header* ph;
    int i;
 
    if (header->magic != ELF_MAGIC) {
        screen << "No valid ELF magic!\n";
        return;
    }
 
    ph = (struct elf_program_header*) (((char*) image) + header->ph_offset);
    for (i = 0; i < header->ph_entry_count; i++, ph++) {
        void* dest = (void*) ph->virt_addr;
        void* src = ((char*) image) + ph->offset;
 
        if (ph->type != 1) {
            continue;
        }
 
        unsigned char* p = (unsigned char*)dest;
        for(int i = 0; i < ph->mem_size; i++) {
                *p = 0;
                p++;
        }
        p = (unsigned char*)dest;
        unsigned char* q = (unsigned char*)src;
        for(int i = 0; i < ph->file_size; i++) {
                *p = *q;
                p++; q++;
        }
    }

    Process newTask;
    newTask.init_task((void (*)()) (header->entry));
}
#endif
