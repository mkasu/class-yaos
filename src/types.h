#ifndef TYPES_H
#define TYPES_H

#define PACKED __attribute__((packed))

typedef unsigned char   uint8_t;
typedef unsigned short  uint16_t;
typedef unsigned int    uint32_t;
typedef unsigned long long uint64_t;

typedef uint32_t uintptr_t;

#define NULL ((void*) 0)

#endif
