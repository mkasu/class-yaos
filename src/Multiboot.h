#ifndef MULTIBOOT_H
#define MULTIBOOT_H

#include "types.h"

#define MULTIBOOT_MAGIC 0x2BADB002
#define NULL ((void*) 0)

struct Multiboot_mmap
{
        uint32_t entry_size;
        uint64_t base;
        uint64_t length;
        uint32_t type;
} PACKED;

struct Multiboot
{
        uint32_t mbs_flags;
        uint32_t mbs_mem_lower;
        uint32_t mbs_mem_upper;
        uint32_t mbs_bootdevce;
        uint32_t mbs_cmdline;
        uint32_t mbs_mods_count;
        void*    mbs_mods_addr;
        uint32_t mbs_syms[4];
        uint32_t mbs_mmap_length;
        Multiboot_mmap*    mbs_mmap_addr;
        /* etc */
} PACKED;


#endif
