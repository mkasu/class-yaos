#include "Video.h"

Video screen;

Video::Video()
{
        videomem = (unsigned char *) 0xb8000;
        off = 0;
        pos = 0;
        color = 0x07;

        clear();
}

Video::~Video()
{
}

void Video::clear() {
        for(int i = 0; i < (80*25); i++) {
                videomem[i*2] = (unsigned char) ' ';
                videomem[i*2+1] = color;
        }

        pos = 0;
        off = 0;
}

Video& Video::operator<<(color::type col) {
        color = (color & ~0x0F) | col;
        return *this;
}

Video& Video::operator<<(const color::background& col) {
	color = (color & ~0xF0) | (col.m_color << 4);
        return *this;
}

Video& Video::operator<<(const char* s) {
        while(*s!=0) put(*s++);
        return *this;
}

Video& Video::operator<<(int n) {
        // not that elegant
        char* buffer = "              ";

        int i = 0;
        int j;
        char tmp;
        unsigned u;

        // handle negative numbers
        if( n < 0 ) {
                buffer[0] = '-';
                buffer++;
                u = ( (unsigned)-(n+1) ) + 1; 
        } else { 
                u = (unsigned)n;
        }
        
        // put the int into the buffer array. note: 1234 will get 4321
        do {
                buffer[i++] = '0' + u % 10;
                u /= 10;
        } while( u > 0 );
 
        // mirror the string
        for( j = 0; j < i / 2; ++j ) {
                tmp = buffer[j];
                buffer[j] = buffer[i-j-1];
                buffer[i-j-1] = tmp;
        }
        
        buffer[i] = '\0';
        screen << buffer;
        return *this;
}

void Video::put(char c) {
        if(pos >= 160) {
                pos = 0;
                off += 160;
        }

        // TODO: do this better!
        if(off >= (160*25)) {
                clear();
        }
        
        if(c == 10) {
                pos = 0;
                off += 160;
                return;
        }

        videomem[off+pos] = (unsigned char) c;
        videomem[off+pos+1] = color;
        pos+=2;
}
