#include "Process.h"
#include "Memory.h"
#include "Video.h"

static struct task* first_task = (task*)0;
static struct task* current_task = (task*)0;

Process::Process() {}
Process::~Process() {}

struct task* Process::init_task(void (*entry)()) {
        stack = (uint8_t*)pmm_alloc();
        user_stack = (uint8_t*)pmm_alloc();

        struct cpu_state new_state;
        new_state.eax = 0;
        new_state.ebx = 0;
        new_state.ecx = 0;
        new_state.edx = 0;
        new_state.esi = 0;
        new_state.edi = 0;
        new_state.ebp = 0;
        new_state.esp = (uint32_t) user_stack + 4096;
        new_state.eip = (uint32_t) entry;

        new_state.cs = 0x08;//0x18 | 0x03;
        //new_state.ss = 0x20 | 0x03;

        new_state.eflags = 0x200;

        struct cpu_state* state = (cpu_state*) (stack + 4096 - sizeof(new_state));
        *state = new_state;

        struct vmm_context* context = (vmm_context*)pmm_alloc();
        task = (struct task*)pmm_alloc();
        task->cpu_state = state;
        task->next = first_task;
        task->context = context;
        first_task = task;

        return task;
}

void task_a() {
        int i = 0;
        while(1) {
                i++;

                if(i > 10000000) {
                        screen << "Process A;  ";
                        i = 0;
                }
        }
}
void task_b() {
        int j = 0;
        while(1) {
                j++;
                if(j > 10000000) {
                        screen << "Process B;  ";
                        j=0;
                }
        }
}
void task_c() {
        int k = 0;
        while(1) {
                k++;
                if(k > 10000000) {
                        screen << "Process C;  ";
                        k=0;
                }
        }
}
void init_multitasking() {
        Process a,b,c,d;
        screen << "Starting Processes A, B and C with Multitasking ...\n\n";

        a.init_task(&task_a);
        b.init_task(&task_b);
        c.init_task(&task_c);
}

struct cpu_state* schedule(struct cpu_state* cpu) {
       // saving old task
       if(current_task != (task*)0) {
                current_task->cpu_state = cpu;
       }

       // change task
       if(current_task == (task*)0) {
                current_task = first_task;
       } else {
                current_task = current_task->next;
                
                // end of array reached
                if(current_task == (task*)0) {
                        current_task = first_task;
                }
       }
       
       struct cpu_state* new_cpu = current_task->cpu_state;

       return new_cpu;
}

void change_task_context() 
{
        //asm volatile("mov %0, %%cr3" : : "r" (current_task->context->pagedir_phys));
}
