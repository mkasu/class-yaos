#ifndef VIDEO_H
#define VIDEO_H

#include "types.h"

namespace color
{
        enum type
        {
                black           = 0x00,
                blue            = 0x01,
                green           = 0x02,
                cyan            = 0x03,
                red             = 0x04,
                magenta         = 0x05,
                brown           = 0x06,
                light_gray      = 0x07,
                dark_gray       = 0x09,
                light_green     = 0x0A,
                light_cyan      = 0x0B,
                light_red       = 0x0C,
                light_magenta   = 0x0D,
                yellow          = 0x0E,
                white           = 0x0F
        };

        struct background
        {
                inline background(type color)
                {
                        m_color = color;
                }
                type m_color;
        };
}



class Video
{
public:
        Video();
        ~Video();
        void clear();
        void put(char c);
        Video& operator<<(const char* s);
        Video& operator<<(color::type color);
        Video& operator<<(const color::background& color);
        Video& operator<<(int n);

private:
        unsigned char* videomem;
        unsigned int off, pos;
        uint16_t color;
};

extern Video screen;

#endif
