global loader
extern kernelMain
extern initialiseConstructors

section .multiboot
FLAGS    equ 0
MAGIC    equ 0x1BADB002
CHECKSUM equ -(MAGIC + FLAGS)

align 4
MultiBootHeader:
        dd MAGIC
        dd FLAGS
        dd CHECKSUM

section .text

loader:
        mov esp,0x200000
        push eax
        push ebx
        call initialiseConstructors
        call kernelMain

stop:
        hlt
        jmp stop
