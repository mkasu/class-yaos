#include "Multiboot.h"
#include "Video.h"
#include "Interrupt.h"
#include "Memory.h"
#include "Process.h"

extern "C" void kernelMain(const Multiboot& multiboot_structure,
                           uint32_t multiboot_magic);

void kernelMain(const Multiboot& multiboot_structure,
                uint32_t multiboot_magic)
{
        if( multiboot_magic != MULTIBOOT_MAGIC ) {
                // something went wrong
                screen << color::red 
                       << "Error 001: Magic Number went wrong!";
        }

        screen << color::blue << "Welcome to YaOS! ";

        screen << color::red << "v0.0 alpha\n\n"
               << color::light_gray << color::background(color::black);

        init_gdt();
        init_interrupts();
        init_pmm(multiboot_structure);
        init_vmm();

        init_multitasking();
}
