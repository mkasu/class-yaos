#!/bin/bash

#----------------------------------
# CONFIG
#----------------------------------
ARCH_CROSS=i386-unknown-elf
PATH_CROSS=~/x-tools/$ARCH_CROSS
#----------------------------------


nasm -f elf32 -o Loader.o src/Loader.asm
nasm -f elf32 -o InterruptAsm.o src/InterruptAsm.asm

$PATH_CROSS/bin/$ARCH_CROSS-gcc -g -m32 -fno-use-cxa-atexit -nostdlib -fno-builtin -fno-rtti -fno-exceptions -fno-leading-underscore -Wall -Wextra -pedantic-errors -std=gnu++0x -c -o Init.o src/Init.cpp

$PATH_CROSS/bin/$ARCH_CROSS-gcc -g -m32 -fno-use-cxa-atexit -nostdlib -fno-builtin -fno-rtti -fno-exceptions -fno-leading-underscore -Wall -Wextra -pedantic-errors -std=gnu++0x -c -o Startup.o src/Startup.cpp

$PATH_CROSS/bin/$ARCH_CROSS-gcc -g -m32 -fno-use-cxa-atexit -nostdlib -fno-builtin -fno-rtti -fno-exceptions -fno-leading-underscore -Wall -Wextra -pedantic-errors -std=gnu++0x -c -o Video.o src/Video.cpp

$PATH_CROSS/bin/$ARCH_CROSS-gcc -g -m32 -fno-use-cxa-atexit -nostdlib -fno-builtin -fno-rtti -fno-exceptions -fno-leading-underscore -Wall -Wextra -pedantic-errors -std=gnu++0x -c -o Interrupt.o src/Interrupt.cpp

$PATH_CROSS/bin/$ARCH_CROSS-gcc -g -m32 -fno-use-cxa-atexit -nostdlib -fno-builtin -fno-rtti -fno-exceptions -fno-leading-underscore -Wall -Wextra -pedantic-errors -std=gnu++0x -c -o Process.o src/Process.cpp

$PATH_CROSS/bin/$ARCH_CROSS-gcc -g -m32 -fno-use-cxa-atexit -nostdlib -fno-builtin -fno-rtti -fno-exceptions -fno-leading-underscore -Wall -Wextra -pedantic-errors -std=gnu++0x -c -o Memory.o src/Memory.cpp


$PATH_CROSS/bin/$ARCH_CROSS-ld -T kernel.ld -o kernel.bin Loader.o Init.o Interrupt.o Memory.o InterruptAsm.o Process.o Startup.o Video.o

#qemu-system-i386 -kernel kernel.bin

