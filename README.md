# yaos v0.0

Small 32-bit kernel project, trying to implement some basic stuff to learn low level programming.

- Mainline source code: <https://github.com/mkasu/yaos>
- Twitter [@mkasu](http://twitter.com/mkasu)

## Build and run

**Get the source code:**

        git clone --recursive https://github.com/mkasu/yaos.git
        git remote add upstream https://github.com/mkasu/yaos.git

**Build:**

        cd yaos
        cmake .
        make

**Run:**

        qemu -kernel kernel.bin


## Development

I'm doing this kernel just for fun, feel free to contribute if you like, but I don't think this project will get any bigger.

## License

This project is licensed under GNU Public License Version 3. To read more about this topic, go to 'COPYING'.

## Contribution

Thanks to:

- Nico aka envy
- R. Kapitza for Operating Systems lecture D:

